//
//  DetailCountryPresenter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class DetailCountryPresenter {
    var view: DetailCountryPresenterToView?
    var interactor: DetailCountryPresenterToInteractor?
    var router: DetailCountryPresenterTiRouter?
}

extension DetailCountryPresenter: DetailCountryViewToPresenter {
    func goToWebViewCountryUbication(url: String) {
        guard let navigationController = (view as? DetailCountryViewController)?.navigationController else { return }
        router?.goToWebViewCountryUbication(url: url, navigation: navigationController)
    }
}
extension DetailCountryPresenter: DetailCountryRouterToPresenter {}
extension DetailCountryPresenter: DetailCountryInteractorToPresenter {}
