//
//  DetailCountryProtocols.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

protocol DetailCountryPresenterToView {}
protocol DetailCountryPresenterToInteractor {}
protocol DetailCountryPresenterTiRouter {
    func goToWebViewCountryUbication(url: String, navigation: UINavigationController)
}
protocol DetailCountryViewToPresenter {
    func goToWebViewCountryUbication(url: String)
}
protocol DetailCountryInteractorToPresenter {}
protocol DetailCountryRouterToPresenter {}
