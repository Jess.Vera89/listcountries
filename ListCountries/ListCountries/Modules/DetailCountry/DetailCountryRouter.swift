//
//  DetailCountryRouter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class DetailCountryRouter {
    var presenter: DetailCountryRouterToPresenter?
    
    public func createModuleDetail(country: CountryResponseModel) -> UIViewController {
        let view = DetailCountryViewController().initWithNibName()
        let presenter = DetailCountryPresenter()
        let interactor = DetailCountryInteractor()
        let router = DetailCountryRouter()
        
        view.presenter = presenter
        view.informationCountry = country
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
    }
}

extension DetailCountryRouter: DetailCountryPresenterTiRouter {
    func goToWebViewCountryUbication(url: String, navigation: UINavigationController) {
        let vc = CountryMapsRouter().createModule(url: url)
        navigation.pushViewController(vc, animated: true)
    }
}
