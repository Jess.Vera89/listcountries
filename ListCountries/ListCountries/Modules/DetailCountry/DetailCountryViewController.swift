//
//  DetailCountryViewController.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class DetailCountryViewController: UIViewController {
    
    static let nibName = "DetailCountryViewController"
    
    
    @IBOutlet weak var nameCountry: UILabel!
    @IBOutlet weak var nameOficial: UILabel!
    @IBOutlet weak var currenciesCountry: UILabel!
    @IBOutlet weak var capitalCountry: UILabel!
    @IBOutlet weak var showImageButton: UIButton!
    @IBOutlet weak var showMapsCountry: UIButton!
    
    var presenter: DetailCountryViewToPresenter?
    var informationCountry: CountryResponseModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleView()
    }
    
    public func initWithNibName() -> DetailCountryViewController {
        return DetailCountryViewController(nibName: nibName, bundle: .localBundle)
    }
    
    func styleView() {
        showImageButton.layer.cornerRadius = 10
        showMapsCountry.layer.cornerRadius = 10
        setDataCountry()
    }
    
    func setDataCountry() {
        print("Pais:", informationCountry?.currencies)
        if let country = informationCountry {
            nameCountry.text = "Nombre común: \(country.name.common)"
            nameOficial.text = "Nombre oficial: \(country.name.official)"
            capitalCountry.text = "Capital: \(country.capital?.first ?? "No disponible")"
            currenciesCountry.text = "Moneda: \(country.currencies?.keys.description ?? "No disponible")"
        }
        
    }
    
    // MARK: - Action Buttons
    
    @IBAction func showImaveCountry(_ sender: UIButton) {
        var popUpWindow: PopUpWindow!
        popUpWindow = PopUpWindow(title: "Bandera", text: informationCountry?.flags.png ?? "", buttontext: "Aceptar")
        self.present(popUpWindow, animated: true, completion: nil)
    }
    
    @IBAction func showMapCountry(_ sender: UIButton) {
        self.presenter?.goToWebViewCountryUbication(url: informationCountry?.maps.googleMaps ?? "")
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
}

extension DetailCountryViewController: DetailCountryPresenterToView {}

//MARK: - PopUpView Delegate
extension DetailCountryViewController: PopUpViewDelegate {
    func closeButton() {
        print("Hola")
    }
}
