//
//  DetailCountryInteractor.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class DetailCountryInteractor {
    var presenter : DetailCountryInteractorToPresenter?
}

extension DetailCountryInteractor: DetailCountryPresenterToInteractor {}
