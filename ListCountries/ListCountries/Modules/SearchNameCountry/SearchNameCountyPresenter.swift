//
//  SearchNameCountyPresenter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class SearchNameCountyPresenter {
    var view: SearchNameCountyPresenterToView?
    var interactor: SearchNameCountyPresenterToInteractor?
    var router: SearchNameCountyPresenterToRouter?
}

extension SearchNameCountyPresenter: SearchNameCountyViewToPresenter {
    func goToDetail(country: CountryResponseModel) {
        guard let navigation = (view as? SearchNameCountyViewController)?.navigationController else { return }
        router?.goToDetail(country: country, navigation: navigation)
    }
    
    func getCountriesData(name: String) {
        interactor?.getCountriesData(name: name)
    }
}
extension SearchNameCountyPresenter: SearchNameCountyInteractorToPresenter {
    func responseData(countries: [CountryResponseModel]) {
        view?.loadData(countries: countries)
    }
    
    func responseErrorData(messaje: String) {
        view?.loadErrorData(messaje: messaje)
    }
}
extension SearchNameCountyPresenter: SearchNameCountyRouterToPresenter {}
