//
//  SearchNameCountyRouter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class SearchNameCountyRouter {
    var presenter: SearchNameCountyRouterToPresenter?
    
    public func createModule() -> UIViewController {
        let view = SearchNameCountyViewController()
        let presenter = SearchNameCountyPresenter()
        let interactor = SearchNameCountyInteractor()
        let router = SearchNameCountyRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
    }
}

extension SearchNameCountyRouter: SearchNameCountyPresenterToRouter {
    func goToDetail(country: CountryResponseModel, navigation: UINavigationController) {
        let vc = DetailCountryRouter().createModuleDetail(country: country)
        navigation.pushViewController(vc, animated: true)
    }
}
