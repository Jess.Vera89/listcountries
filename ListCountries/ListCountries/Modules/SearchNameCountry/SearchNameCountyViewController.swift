//
//  SearchNameCountyViewController.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class SearchNameCountyViewController: UIViewController {
    
    static let nibName = "SearchNameCountyViewController"
    
    @IBOutlet weak var nameContryTextField: UITextField!
    @IBOutlet weak var searchcountryButton: UIButton!
    @IBOutlet weak var countyTable: UITableView!
    
    var presenter: SearchNameCountyViewToPresenter?
    var arrayCountries: [CountryResponseModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.nameContryTextField.text = ""
        self.nameContryTextField.placeholder = "Nombre"
        self.arrayCountries.removeAll()
        self.countyTable.reloadData()
    }
    
    public func initWithNibName() -> SearchNameCountyViewController {
        return SearchNameCountyViewController(nibName: nibName, bundle: .localBundle)
    }
    
    func styleView() {
        nameContryTextField.placeholder = "Nombre"
        searchcountryButton.layer.cornerRadius = 10
        searchcountryButton.addTarget(self, action: #selector(searchNameCountry), for: .touchUpInside)
        tableConfiguration()
    }
    
    func tableConfiguration() {
        countyTable.delegate = self
        countyTable.dataSource = self
        countyTable.rowHeight = 60
        countyTable.register(UINib(nibName: "CountriesTableViewCell", bundle: .localBundle), forCellReuseIdentifier: "cellCountryName")
    }
    
    // MARK: - Action Button
    @objc func searchNameCountry(sender: UIButton!) {
        
        if nameContryTextField.text == "" {
            showSimpleAlert(title: "Alerta", message: "Ingresa el nombre del pais.")
        } else{
            showGenericSpinner()
            presenter?.getCountriesData(name: nameContryTextField.text!)
        }
    }
}

extension SearchNameCountyViewController: SearchNameCountyPresenterToView {
    func loadData(countries: [CountryResponseModel]) {
        removeGenericSpinner()
        arrayCountries = countries
        countyTable.reloadData()
    }
    
    func loadErrorData(messaje: String) {
        removeGenericSpinner()
        showSimpleAlert(title: "Advertencia", message: messaje)
        countyTable.reloadData()
    }
}

// MARK: - Delegate UITableView
extension SearchNameCountyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCountries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellCountry = tableView.dequeueReusableCell(withIdentifier: "cellCountryName", for: indexPath) as! CountriesTableViewCell
        let countyData = arrayCountries[indexPath.row]
        cellCountry.setData(country: countyData)
        print(countyData)
        return cellCountry
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectCounty = arrayCountries[indexPath.row]
        presenter?.goToDetail(country: selectCounty)
        
    }
}
