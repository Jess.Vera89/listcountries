//
//  SearchNameCountyProtocols.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

protocol SearchNameCountyPresenterToView {
    func loadData(countries: [CountryResponseModel])
    func loadErrorData(messaje: String)
}

protocol SearchNameCountyPresenterToInteractor {
    func getCountriesData(name: String)
}
protocol SearchNameCountyPresenterToRouter {
    func goToDetail(country: CountryResponseModel, navigation: UINavigationController)
}

protocol SearchNameCountyViewToPresenter {
    func getCountriesData(name: String)
    func goToDetail(country: CountryResponseModel)
}

protocol SearchNameCountyInteractorToPresenter {
    func responseData(countries: [CountryResponseModel])
    func responseErrorData(messaje: String)
}

protocol SearchNameCountyRouterToPresenter {}
