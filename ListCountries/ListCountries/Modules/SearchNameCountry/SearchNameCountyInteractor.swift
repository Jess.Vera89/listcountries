//
//  SearchNameCountyInteractor.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class SearchNameCountyInteractor {
    var presenter: SearchNameCountyInteractorToPresenter?
}

extension SearchNameCountyInteractor: SearchNameCountyPresenterToInteractor {
    func getCountriesData(name: String) {
        let url = "https://restcountries.com/v3.1/name/" + name
        let urlComplete = URL(string: url)!
        
        var request = URLRequest(url: urlComplete)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard let self = self else { return }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                self.presenter?.responseErrorData(messaje: "Respuesta Invalida")
                return
            }

            let statusCode = httpResponse.statusCode
            
            if let error = error {
                print("Error:", error)
                DispatchQueue.main.async {
                    self.presenter?.responseErrorData(messaje: "Error al obtener los datos")
                }
                
                return
            }
            
            guard let data = data else {
                print("No se recibieron Datos")
                DispatchQueue.main.async {
                    self.presenter?.responseErrorData(messaje: "No se recibieron Datos")
                }
                return
            }
            
            do {
                let countries = try JSONDecoder().decode([CountryResponseModel].self, from: data)
                DispatchQueue.main.async { // Update UI on main thread
                    self.presenter?.responseData(countries: countries)
                }
            } catch {
                print("Error al decodificar JSON:", error)
                DispatchQueue.main.async {
                    switch statusCode {
                    case 300...399:
                        self.presenter?.responseErrorData(messaje: "Error al realizar peticion")
                    case 400...499:
                        self.presenter?.responseErrorData(messaje: "No hay coincidencias")
                    case 500...599:
                        self.presenter?.responseErrorData(messaje: "Servicio no disponible")
                    default:
                        self.presenter?.responseErrorData(messaje: "Unexpected status code: \(statusCode)")
                    }
                }
            }
        }
        
        task.resume()
    }
}



import Foundation

func makeRequest(to url: URL, completion: @escaping (Data?, Error?) -> Void) {
    let session = URLSession.shared
    var request = URLRequest(url: url)
    request.httpMethod = "GET" 

    let task = session.dataTask(with: request) { data, response, error in
        if let error = error {
            completion(nil, error)
            return
        }

        guard let httpResponse = response as? HTTPURLResponse else {
            completion(nil, NSError(domain: "NetworkingError", code: 0, userInfo: ["message": "Invalid response"]))
            return
        }

        let statusCode = httpResponse.statusCode
        
    }
    task.resume()
}

