//
//  SearchOptionRouter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class SearchOptionRouter {
    var presenter: SearchOptionRouterToPresenter?
    
    public func createModuleSearchOption(window: UIWindow?) -> UIViewController {
        let view = SearchOptionViewController().initWithNibName()
        let presenter = SearchOptionPresenter()
        let interactor = SearchOptionInteractor()
        let router = SearchOptionRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        let navigation = UINavigationController()
        navigation.viewControllers = [view]
        window?.rootViewController = navigation
        window?.makeKeyAndVisible()
        
        return view
    }
}

extension SearchOptionRouter : SearchOptionPresenterToRouter {
    func goToAllCountries(navigationController: UINavigationController) {
        let vc = ListContriesRouter().createModuleListContries()
        navigationController.pushViewController(vc, animated: true)
    }
    
    func goToNameCountries(navigationController: UINavigationController) {
        let vc = SearchNameCountyRouter().createModule()
        navigationController.pushViewController(vc, animated: true)
    }
}
