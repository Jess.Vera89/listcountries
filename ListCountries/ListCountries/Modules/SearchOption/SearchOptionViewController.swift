//
//  SearchOptionViewController.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class SearchOptionViewController: UIViewController {
    static let nibName = "SearchOptionViewController"
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var allCountriesButton: UIButton!
    @IBOutlet weak var nameCountriesButton: UIButton!
    
    var presenter: SearchOptionViewToPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleView()

    }
    
    public func initWithNibName() -> SearchOptionViewController {
        return SearchOptionViewController(nibName: nibName, bundle: .localBundle)
    }
    
    func styleView() {
        contentView.layer.cornerRadius = 10
        allCountriesButton.layer.cornerRadius = 10
        nameCountriesButton.layer.cornerRadius = 10
        
        allCountriesButton.addTarget(self, action: #selector(searchOptionAllCountries), for: .touchUpInside)
        nameCountriesButton.addTarget(self, action: #selector(searchOptionNameCountries), for: .touchUpInside)
    }

    //MARK: ActionButtons
    @objc func searchOptionAllCountries(sender: UIButton!) {
        presenter?.goToAllCountries()
    }
    
    @objc func searchOptionNameCountries(sender: UIButton!) {
        presenter?.goToNameCountries()
    }

}

extension SearchOptionViewController: SearchOptionPresenterToView {}
