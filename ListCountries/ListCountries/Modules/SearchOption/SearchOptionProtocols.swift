//
//  SearchOptionProtocols.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

protocol SearchOptionViewToPresenter {
    func goToAllCountries()
    func goToNameCountries()
}
protocol SearchOptionInteractorToPresenter {}
protocol SearchOptionRouterToPresenter {}
protocol SearchOptionPresenterToView {}
protocol SearchOptionPresenterToInteractor {}
protocol SearchOptionPresenterToRouter {
    func goToAllCountries(navigationController: UINavigationController)
    func goToNameCountries(navigationController: UINavigationController)
}

