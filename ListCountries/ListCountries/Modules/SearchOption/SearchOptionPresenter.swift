//
//  SearchOptionPresenter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class SearchOptionPresenter {
    var view: SearchOptionPresenterToView?
    var interactor: SearchOptionPresenterToInteractor?
    var router: SearchOptionPresenterToRouter?
}

extension SearchOptionPresenter: SearchOptionViewToPresenter {
    func goToAllCountries() {
        guard let navigationController = (view as? SearchOptionViewController)?.navigationController else { return }
        router?.goToAllCountries(navigationController: navigationController)
    }
    
    func goToNameCountries() {
        guard let navigationController = (view as? SearchOptionViewController)?.navigationController else { return }
        router?.goToNameCountries(navigationController: navigationController)
    }
}
extension SearchOptionPresenter: SearchOptionInteractorToPresenter {}
extension SearchOptionPresenter: SearchOptionRouterToPresenter {}
