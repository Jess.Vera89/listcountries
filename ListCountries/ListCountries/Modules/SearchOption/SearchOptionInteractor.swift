//
//  SearchOptionInteractor.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class SearchOptionInteractor {
    var presenter: SearchOptionInteractorToPresenter?
}

extension SearchOptionInteractor: SearchOptionPresenterToInteractor {}
