//
//  CountriesTableViewCell.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class CountriesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(country: CountryResponseModel) {
        countryName.text = country.name.official
        countryImage.loadString(url: URL(string: country.flags.png)!)
    }
    
}
