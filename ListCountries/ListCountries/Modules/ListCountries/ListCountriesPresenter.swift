//
//  ListCountriesPresenter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class ListContriesPresenter {
    var view        : ListContriesPresenterToView?
    var interactor  : ListContriesPresenterToInteractor?
    var router      : ListContriesPresenterToRouter?
}

extension ListContriesPresenter: ListContriesViewToPresenter {
    func goToDetail(country: CountryResponseModel) {
        guard let navigationController = (view as? ListCountriesViewController)?.navigationController else { return }
        router?.goToDetail(country: country, navigation: navigationController)
    }
    
    func getCountriesData() {
        interactor?.getCountriesData()
    }
}
extension ListContriesPresenter: ListContriesInteractorToPresenter {
    func responseData(countries: [CountryResponseModel]) {
        view?.loadData(countries: countries)
    }
    
    func responseErrorData(messaje: String) {
        view?.loadErrorData(messaje: messaje)
    }
}
extension ListContriesPresenter: ListContriesRouterToPresenter {}
