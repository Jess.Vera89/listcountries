//
//  ListCountriesProtocols.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

protocol ListContriesPresenterToView {
    func loadData(countries: [CountryResponseModel])
    func loadErrorData(messaje: String)
}
protocol ListContriesPresenterToInteractor {
    func getCountriesData()
}
protocol ListContriesPresenterToRouter {
    func goToDetail(country: CountryResponseModel, navigation: UINavigationController)
}

protocol ListContriesViewToPresenter {
    func getCountriesData()
    func goToDetail(country: CountryResponseModel)
}
protocol ListContriesInteractorToPresenter {
    func responseData(countries: [CountryResponseModel])
    func responseErrorData(messaje: String)
}
protocol ListContriesRouterToPresenter {}
