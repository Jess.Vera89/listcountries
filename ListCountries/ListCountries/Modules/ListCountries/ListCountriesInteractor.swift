//
//  ListCountriesInteractor.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class ListContriesInteractor {
    var presenter: ListContriesInteractorToPresenter?
}

extension ListContriesInteractor: ListContriesPresenterToInteractor {
    func getCountriesData() {
        let url = URL(string: "https://restcountries.com/v3.1/all")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard let self = self else { return }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                self.presenter?.responseErrorData(messaje: "Respuesta Invalida")
                return
            }

            let statusCode = httpResponse.statusCode
            
            if let error = error {
                print("Error:", error)
                DispatchQueue.main.async {
                    self.presenter?.responseErrorData(messaje: "Error al obtener los datos")
                }
                
                return
            }
            
            guard let data = data else {
                print("No se recibieron Datos")
                DispatchQueue.main.async {
                    self.presenter?.responseErrorData(messaje: "No se recibieron Datos")
                }
                return
            }
            
            do {
                let countries = try JSONDecoder().decode([CountryResponseModel].self, from: data)
                DispatchQueue.main.async {
                    self.presenter?.responseData(countries: countries)
                }
            } catch {
                print("Error al decodificar JSON:", error)
                DispatchQueue.main.async {
                    switch statusCode {
                    case 300...399:
                        self.presenter?.responseErrorData(messaje: "Error al realizar peticion")
                    case 400...499:
                        self.presenter?.responseErrorData(messaje: "No hay coincidencias")
                    case 500...599: 
                        self.presenter?.responseErrorData(messaje: "Servicio no disponible")
                    default:
                        self.presenter?.responseErrorData(messaje: "Unexpected status code: \(statusCode)")
                    }
                }
            }
        }
        
        task.resume()
    }
}
