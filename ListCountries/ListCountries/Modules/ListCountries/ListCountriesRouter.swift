//
//  ListCountriesRouter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class ListContriesRouter {
    var presenter: ListContriesRouterToPresenter?
    
    public func createModuleListContries() -> UIViewController {
        let view = ListCountriesViewController().initWithNibName()
        let presenter = ListContriesPresenter()
        let interactor = ListContriesInteractor()
        let router = ListContriesRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
    }
}

extension ListContriesRouter : ListContriesPresenterToRouter {
    func goToDetail(country: CountryResponseModel, navigation: UINavigationController) {
        let vc = DetailCountryRouter().createModuleDetail(country: country)
        navigation.pushViewController(vc, animated: true)
    }
}
