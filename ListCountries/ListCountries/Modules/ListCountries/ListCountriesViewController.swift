//
//  ListCountriesViewController.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

class ListCountriesViewController: UIViewController {
    
    static let nibName = "ListCountriesViewController"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countriesTable: UITableView!
    
    var presenter: ListContriesViewToPresenter?
    var arrayCountries: [CountryResponseModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleView()
        showGenericSpinner()
        presenter?.getCountriesData()
        
    }
    
    func initWithNibName() -> ListCountriesViewController {
        return ListCountriesViewController(nibName: nibName, bundle: .localBundle)
    }
    
    func styleView() {
        
        
        titleLabel.text = "Todos los paises."
        
        tableConfiguration()
        
    }
    
    func tableConfiguration() {
        countriesTable.delegate = self
        countriesTable.dataSource = self
        countriesTable.rowHeight = 60
        countriesTable.register(UINib(nibName: "CountriesTableViewCell", bundle: .localBundle), forCellReuseIdentifier: "cellCountry")
    }
    
}

extension ListCountriesViewController: ListContriesPresenterToView {
    func loadData(countries: [CountryResponseModel]) {
        removeGenericSpinner()
        arrayCountries = countries
        countriesTable.reloadData()
    }
    
    func loadErrorData(messaje: String) {
        removeGenericSpinner()
        showSimpleAlert(title: "Advertencia", message: messaje)
    }
}
// MARK: - Delegate UITableView
extension ListCountriesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCountries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellCountry = tableView.dequeueReusableCell(withIdentifier: "cellCountry", for: indexPath) as! CountriesTableViewCell
        let countyData = arrayCountries[indexPath.row]
        cellCountry.setData(country: countyData)
        return cellCountry
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectCounty = arrayCountries[indexPath.row]
        presenter?.goToDetail(country: selectCounty)
        
    }
}
