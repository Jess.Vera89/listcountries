//
//  CountryMapsProtocols.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import Foundation

protocol CountryMapsPresenterToView {}
protocol CountryMapsPresenterToInteractor {}
protocol CountryMapsPresenterToRouter {}
protocol CountryMapsViewToPresenter {}
protocol CountryMapsInteractorToPresenter {}
protocol CountryMapsRouterToPresenter {}
