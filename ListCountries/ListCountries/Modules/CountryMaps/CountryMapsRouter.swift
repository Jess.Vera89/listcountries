//
//  CountryMapsRouter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class CountryMapsRouter {
    var presenter: CountryMapsRouterToPresenter?
    
    public func createModule(url: String) -> UIViewController {
        let view = CountryMapsViewController().initWithNibName()
        let presenter = CountryMapsPresenter()
        let interactor = CountryMapsInteractor()
        let router = CountryMapsRouter()
        
        view.presenter = presenter
        view.urlUbicationContry = url
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
    }
}


extension CountryMapsRouter: CountryMapsPresenterToRouter {}
