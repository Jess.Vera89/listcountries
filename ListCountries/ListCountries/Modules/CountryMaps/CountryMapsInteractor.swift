//
//  CountryMapsInteractor.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class CountryMapsInteractor {
    var presenter: CountryMapsInteractorToPresenter?
}

extension CountryMapsInteractor: CountryMapsPresenterToInteractor {}
