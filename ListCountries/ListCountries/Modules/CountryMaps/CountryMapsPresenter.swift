//
//  CountryMapsPresenter.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

class CountryMapsPresenter {
    var view: CountryMapsPresenterToView?
    var interactor: CountryMapsPresenterToInteractor?
    var router: CountryMapsPresenterToRouter?
}

extension CountryMapsPresenter: CountryMapsViewToPresenter {}
extension CountryMapsPresenter: CountryMapsInteractorToPresenter {}
extension CountryMapsPresenter: CountryMapsRouterToPresenter {}
