//
//  CountryMapsViewController.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit
import WebKit

class CountryMapsViewController: UIViewController {
    
    static let nibName = "CountryMapsViewController"
    var presenter: CountryMapsViewToPresenter?
    var webView: WKWebView!
    var urlUbicationContry = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleView()
    }
    
    public func initWithNibName() -> CountryMapsViewController {
        return CountryMapsViewController(nibName: nibName, bundle: .localBundle)
    }
    
    func styleView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
        webViewConfiguration()
    }
    
    func webViewConfiguration() {
        if urlUbicationContry != "" {
            let url = URL(string: urlUbicationContry)!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
        } else {
            let url = URL(string: "https://www.google.com")!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
        }
       
    }
}

extension CountryMapsViewController: CountryMapsPresenterToView {}

extension CountryMapsViewController: WKNavigationDelegate {
    
}
