//
//  ResponseModels.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 02/05/24.
//

import UIKit

struct CountryResponseModel: Codable {
  let name: Name
  let currencies: [String: Currency]?
  let capital: [String]?
  let region: String
  let subregion: String?
  let languages: [String: String]?
  let area: Double
  let flag: String
  let maps: Maps
  let population: Int
  let gini: [String: Double]? // Optional for nested object
  let fifa: String?
  let continents: [String]
  let flags: Flags
}

struct Name: Codable {
  let common: String
  let official: String
  let nativeName: [String: NativeName]? // Optional for nested object
}

struct NativeName: Codable {
  let official: String
  let common: String
}

struct Currency: Codable {
  let name: String
  let symbol: String?
}

struct Maps: Codable {
  let googleMaps: String
  let openStreetMaps: String
}

struct Flags: Codable {
  let png: String
  let svg: String
  let alt: String?
}
