//
//  PopUpView.swift
//  ListCountries
//
//  Created by Jessica Vera Perez on 03/05/24.
//

import UIKit

protocol PopUpViewDelegate: AnyObject {
    func closeButton()
}

class PopUpView: UIView {
    let popupView = UIView(frame: CGRect.zero)
    let popupTitle = UILabel(frame: CGRect.zero)
    let popupImage = UIImageView(frame: CGRect.zero)
    let popupButton = UIButton(frame: CGRect.zero)
    
    let BorderWidth: CGFloat = 2.0
    
    init() {
        super.init(frame: CGRect.zero)
        // Semi-transparent background
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        // Popup Background
        popupView.backgroundColor = .white
        popupView.layer.borderWidth = BorderWidth
        popupView.layer.masksToBounds = true
        popupView.layer.borderColor = UIColor.white.cgColor
        
        // Popup Title
        popupTitle.textColor = .black
        popupTitle.backgroundColor = .white
        popupTitle.layer.masksToBounds = true
        popupTitle.adjustsFontSizeToFitWidth = true
        popupTitle.clipsToBounds = true
        popupTitle.font = UIFont.systemFont(ofSize: 23.0, weight: .bold)
        popupTitle.numberOfLines = 1
        popupTitle.textAlignment = .center
        
        // Popup Button
        popupButton.layer.cornerRadius = 10
        popupButton.setTitleColor(.white, for: .normal)
        popupButton.titleLabel?.font = UIFont.systemFont(ofSize: 23.0, weight: .bold)
        popupButton.backgroundColor = UIColor.blue
        
        popupView.addSubview(popupTitle)
        popupView.addSubview(popupImage)
        popupView.addSubview(popupButton)
        
        addSubview(popupView)
        
        // PopupView constraints
        popupView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            popupView.heightAnchor.constraint(equalToConstant: 400),
            popupView.widthAnchor.constraint(equalToConstant: 300),
            popupView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            popupView.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
        
        // PopupTitle constraints
        popupTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            popupTitle.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 10),
            popupTitle.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -10),
            popupTitle.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 10),
            popupTitle.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        // PopupImage contraints
        popupImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            popupImage.topAnchor.constraint(equalTo: popupTitle.bottomAnchor, constant: 8),
            popupImage.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 8),
            popupImage.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -8),
            popupImage.bottomAnchor.constraint(equalTo: popupButton.topAnchor, constant: -8)
        ])
        
        // PopupButton constraints
        popupButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            popupButton.heightAnchor.constraint(equalToConstant: 60),
            popupButton.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 10),
            popupButton.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -10),
            popupButton.bottomAnchor.constraint(equalTo: popupView.bottomAnchor, constant: -10)
        ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
